import os
import urllib2, json, base64

ARIA_SERVER_URL = "http://localhost:6800/jsonrpc"

jsonreq = json.dumps({'jsonrpc':'2.0', 'id':'qwer',
                       'method':'aria2.addUri',
                       'params':[['http://example.org/file']]})

def seed_torrent(torrent_path, download_dir):
	torrent_path = os.path.abspath(torrent_path)
	download_dir = os.path.abspath(download_dir)

	print "torrent path: %s\ndownload_dir: %s\n" % (torrent_path, download_dir)
	dl_id = os.path.basename(torrent_path)

	torrent = base64.b64encode(open(torrent_path, 'rb').read()).encode('ascii')

	opts = {    'dir':download_dir,
				'check-integrity':True,
				'bt-hash-check-seed':True}

	jsonreq = json.dumps({'jsonrpc':'2.0', 
							'id':dl_id,
						   'method':'aria2.addTorrent', 
						   'params':[torrent, [], opts]})

	print "JSON: " + jsonreq

	c = urllib2.urlopen(ARIA_SERVER_URL, jsonreq)
	return c

