import os
import random
import hashlib
import base64

import funutil

def user_random_color(name):
	random.seed(name)
	r = lambda: random.randint(0,128)
	return '#%02X%02X%02X' % (r(),r(),r())

def get_name_hash(name):
	md = hashlib.md5(name)
	return base64.urlsafe_b64encode(md.digest())

def get_piece_url(pack, piece):
	return '/static/up/%s/%s/%s' % (pack['uploader_hash'], pack['name'], piece['name'])

def get_upload_folder(folder, ip_hash, packname):
	upload_folder = os.path.join(folder, ip_hash)
	upload_folder = os.path.join(upload_folder, packname)
	upload_folder = os.path.normcase(upload_folder)

	return upload_folder

def get_user_folder(folder, ip_hash):
	user_folder = os.path.join(folder, ip_hash)
	user_folder = os.path.normcase(user_folder)

	return user_folder


