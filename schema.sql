drop table if exists pack;
drop table if exists piece;

CREATE TABLE pack (
    id integer PRIMARY KEY AUTOINCREMENT, 
    name    varchar NOT NULL, 
    description varchar,
    upload_date DATETIME DEFAULT CURRENT_TIMESTAMP,
    uploader_ip varchar NOT NULL,
    uploader_hash varchar NOT NULL,
    torrent_url varchar,
    zip_url varchar
);

create table piece (
    name varchar NOT NULL,
    size integer,
    parent integer references pack (id),
    PRIMARY KEY(name, parent)
);
