import os
import hashlib
import argparse
import flask
from flask import Flask, redirect, url_for, render_template, request, flash, g
from werkzeug import secure_filename
import arialib

import db
from db import query_db
import users
import funutil

app = Flask(__name__)
app.secret_key = 'lol_SEC:::RET'

LOCAL_HOST='127.0.0.1'
UPLOAD_FOLDER = 'static/up'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['ADMIN_IPS'] = ['127.0.0.1']

app.jinja_env.globals.update(url_for=flask.url_for)
app.jinja_env.globals.update(get_pack_url=db.get_pack_url)
app.jinja_env.globals.update(users=users)
app.jinja_env.globals.update(funutil=funutil)
app.jinja_env.globals.update(splitext=os.path.splitext)
app.jinja_env.globals.update(stylepath="/static/style.css")

def is_admin(request):
	if request.remote_addr in app.config['ADMIN_IPS']:
		return True

	return False

@app.route("/")
def main():
	packs = query_db('select id, pack.name, description, upload_date, uploader_ip, uploader_hash, torrent_url, zip_url, count(piece.name) as cnt from pack, piece \
			where piece.parent = pack.id\
			group by pack.id\
			order by upload_date DESC;')

	return render_template('upload.html', request = request, users = users, packs=packs)
	
@app.route("/user/<userid>/<packname>")
def packview(userid, packname):
	pack = db.get_pack(userid, packname)

	if not pack:
		flash('No such pack!', 'error')
		return render_template('error.html')

	print "jajaj " + str(pack['id'])
	pieces = query_db('select * from piece WHERE parent = ?;', (str(pack['id']), ))

	admin = is_admin(request)

	return render_template('pack.html', userid = userid, pack = pack, pieces=pieces, admin=admin)

@app.route('/upload', methods=['POST'])
def upload_file():
	packname = ''
	description = ''
	dozip = False
	dotorrent = False

	if 'packname' in request.form:
		packname = request.form['packname']
		packname = os.path.basename(db.clean_filename(packname))

	if len(packname) == 0:
		packname = db.time_filename()

	if 'description' in request.form:
		description = request.form['description']

	if 'dozip' in request.form:
		dozip = request.form['dozip'] == 'zip'

	if 'dotorrent' in request.form:
		dotorrent = request.form['dotorrent'] == 'torrent'

	filelist = request.files.getlist('up_files')
	print "uploaded files: " + repr(filelist)
	#print "flist: " + str(len(filelist))
	#file = request.files['up_files']
	if len(filelist) == 0:
		return "pls upload files"

	ip_addr = request.remote_addr 
	ip_addr_hash = users.get_name_hash(request.remote_addr)
	print ip_addr_hash

	user_folder = users.get_user_folder(app.config['UPLOAD_FOLDER'], ip_addr_hash)
	upload_folder = users.get_upload_folder(app.config['UPLOAD_FOLDER'], ip_addr_hash, packname)

	if not os.path.exists(upload_folder):
		os.makedirs(upload_folder)

	query_db('insert into pack (name,description,uploader_ip,uploader_hash) \
			VALUES (?, ?, ?, ?)', (packname, description, ip_addr, ip_addr_hash))
	packid = query_db('select id from pack where name = ? and uploader_ip = ?', 
			(packname, ip_addr), True)['id']

	#if file and allowed_file(file.filename):
	for file in filelist:
		filename = secure_filename(file.filename)
		file_full_name = os.path.join(upload_folder, filename)
		print "saving " + filename
		print "to " + upload_folder

		if (os.path.exists(file_full_name)):
			print "overwriting file %s " % filename
			flash("overwriting file %s" % filename)

		file.save(file_full_name)
		filesize = os.stat(file_full_name).st_size
		query_db('insert into piece (name, size, parent) VALUES (?, ?, ?)',
				(filename, str(filesize), packid))

	if dozip:
		zip_url = upload_folder + '.zip'
		funutil.zip_dir_content(upload_folder, zip_url)
		query_db('update pack set zip_url = ? where id = ?;', (zip_url, packid))
		flash('created zip archive')
	
	if dotorrent:
		torrent_path = upload_folder + '.torrent'
		ret = funutil.create_torrent(upload_folder, torrent_path)
		query_db('update pack set torrent_url = ? where id = ?;', (torrent_path, packid))


		try:
			arialib.seed_torrent(torrent_path, user_folder)
		except Exception as e:
			flash('cannot seed! ' + str(e))
		flash('probably created a torrent, code: %s' % str(ret))
	
	flash("files probably uploaded fine")
	return redirect(url_for('packview', userid=ip_addr_hash, packname=packname))


@app.route('/thankyou')
def uploaded_file():
		return '<a href="/">thanks!</a>'

@app.route('/delete/<userid>/<packname>/<filename>')
def delete_file(userid, packname, filename):
	if not is_admin(request):
		abort(403)

	upload_folder = users.get_upload_folder(app.config['UPLOAD_FOLDER'], userid, packname)
	path = os.path.join(upload_folder, filename)

	if os.path.exists(path):
		os.remove(path)

	pack = db.get_pack(userid, packname)

	if not pack:
		flash('%s ei loeytny kannasta!' % packname)
		return redirect(url_for('main'))

	query_db('delete from piece where piece.name = ? and piece.parent = ?', 
			(filename, pack['id']))

	flash('file %s poistettu!' % filename)
	return redirect(url_for('main'))

@app.route('/force_seed/<userid>/<packname>')
def force_seed(userid, packname):
	if not is_admin(request):
		abort(403)

	upload_folder = users.get_upload_folder(app.config['UPLOAD_FOLDER'], userid, packname)
	user_folder = users.get_user_folder(app.config['UPLOAD_FOLDER'], userid)
	torrent_path = upload_folder + '.torrent'

	try:
		arialib.seed_torrent(torrent_path, user_folder)
	except Exception as e:
		flash('cannot seed! ' + str(e))
	else:
		flash('seeding!')

	return redirect(url_for('packview', userid=userid, packname=packname))

"""
@app.route('/up/<userid>/<packname>/<filename>')
def download_file(userid, packname, filename):
	userid = os.path.basename(userid)
	packname = os.path.basename(packname)
	filename = os.path.basename(filename)

	path = "%s/%s/%s.%s" % (app.config['UPLOAD_FOLDER'], userid, packname, filename)
	
	if not os.file.exists(path)
		flash('No such file!', 'error')
		return render_template('error.html')
"""

@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()


if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument('-i', '--init', action='store_true')
	parser.add_argument('-r', '--reset', action='store_true')
	parser.add_argument('-a', '--host', action='store')
	args=parser.parse_args()

	if args.reset:
		db.drop_db(app)

	if args.init:
		db.create(app)

	host_ip = LOCAL_HOST

	if args.host:
		host_ip = args.host
		print "Binding to " + host_ip

	app.run(debug=True, host=host_ip)
