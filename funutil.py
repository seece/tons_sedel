import os
import zipfile
import subprocess

def sizeof_fmt(num):
    for x in ['bytes','KB','MB','GB','TB']:
        if num < 1024.0:
            return "%3.1f %s" % (num, x)
        num /= 1024.0

def zipdir(path, zip):
    for root, dirs, files in os.walk(path):
        for file in files:
            zip.write(os.path.join(root, file), os.path.basename(file))

def zip_dir_content(source_dir, output_path):
	zip = zipfile.ZipFile(output_path, 'w')
	zipdir(source_dir, zip)
	zip.close()

mktorrent_path = 'tools/mktorrent/mktorrent.exe'
tracker_url = "udp://tracker.openbittorrent.com:80/announce"

def create_torrent(source_dir, output_path):
	retcode = subprocess.call([mktorrent_path, '--announce=%s' % tracker_url, '--output=%s' % output_path, source_dir], shell=False)	
	return retcode 

def path_to_url(path):
	return path.replace('\\', '/')
