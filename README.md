## Introduction

A simple web interface for sharing files over local network using HTTP and Torrent-protocol. Files uploaded are compressed to a single zip archive for easy download, and optionally wrapped into a torrent which is then automatically seeded with [aria2](http://aria2.sourceforge.net/)

## Installation instructions

### Requirements

Currently this is only tested on Windows 7, but it should run anywhere where mktorrent, aria2 and Python are available.

### Downloads

Download mktorrent to tools/mktorrent from http://str0.at/wp-content/uploads/2009/11/mktorrent-win.rar

Download aria2 and place it to tools/aria2 http://aria2.sourceforge.net/

## Configuration

Use the following config with aria:

	daemon=true
	enable-rpc=true
	file-allocation=none
	seed-time=99999
	seed-ratio=100.0
	enable-dht=true
	log=aria2-0.log
	log-level=notice
	bt-enable-lpd=true
	bt-hash-check-seed=true
	bt-save-metadata=false
	bt-seed-unverified=true
	rpc-save-upload-metadata=false
	
Then start aria with 

	aria2c --conf-path=aria.conf


## Release mode
On release you probably want to bind the flask host to ´0.0.0.0´