import os
import sqlite3
from time import gmtime, strftime
from flask import g

DB_PATH = 'sedeltons.db'

def get_pack_url(pack):
	return "/user/%s/%s" % (pack['uploader_hash'], pack['name'])

def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DB_PATH)

	db.row_factory = sqlite3.Row
    return db

def drop_db(app):
	pass
	"""
	if os.path.exists(DB_PATH):
		os.remove(DB_PATH)
	else:
		print "can't remove db, it doesn't exist!"
	""" 

def create(app):
	print "creating db!"

	with app.app_context():
			db = get_db()
			with app.open_resource('schema.sql', mode='r') as f:
				db.cursor().executescript(f.read())
			db.commit()
		

def clean_filename(filename):
	filename = filename.replace(' ', '_')
	return "".join([c for c in filename if c.isalpha() or c.isdigit() or c=='_']).rstrip()

def time_filename():
	return strftime("%Y-%m-%d-%H%M%S", gmtime())

def query_db(query, args=(), one=False):
	cur = get_db().execute(query, args)
	get_db().commit()
	rv = cur.fetchall()
	cur.close()
	return (rv[0] if rv else None) if one else rv

def get_pack(userid, packname):
	pack = query_db('select * from pack WHERE uploader_hash = ? \
			AND name = ?;', (userid, packname), True)
	return pack
